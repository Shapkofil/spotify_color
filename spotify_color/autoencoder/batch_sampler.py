import numpy as np 
import random 
import torch 
from dataset import Dataset
from typing import *

class BatchSampler:
    """
    An iterable for training and test. 
    """
    def __init__(self, batch_size: int, dataset: Dataset) -> None:
        self.batch_size = batch_size 
        self.dataset = dataset
        self.indexes = [i for i in range(len(dataset))]
    
    def __len__(self) -> int:
        return(len(self.indexes) // self.batch_size) + 1 
    
    def shuffle(self) -> None: 
        random.shuffle(self.indexes)
    
    def __iter__(self) -> Generator[Tuple[torch.Tensor, torch.Tensor], None, None]:
        self.shuffle()
        remaining = False 
        # Go over the dataset in steps of 'self.batch_size'
        for i in range(0, len(self.indexes), self.batch_size):
            # If our current batch is larger than the remaining data, we quit:
            if i + self.batch_size > len(self.indexes):
                remaining = True 
                break 
            # If not, we yield a complete batch:
            else: 
                X_batch = [
                    self.dataset[self.indexes[k]][0] 
                    for k in range(i, i + self.batch_size)
                ]
                Y_batch = [
                    self.dataset[self.indexes[k]][1]
                    for k in range(i, i + self.batch_size)
                ]
                
                yield torch.stack(X_batch).float(), torch.stack(Y_batch).float()
        # if there is still data left that was not a full batch: 
        if remaining:
            # Return the last batch (smaller than batch_size):
            X_batch = [
                self.dataset[self.indexes[k]][0] 
                for k in range(i, len(self.indexes))
            ]
            Y_batch = [
                self.dataset[self.indexes[k]][0]
                for k in range(i, len(self.indexes))
            ]
            yield torch.stack(X_batch).float(), torch.stack(Y_batch).float()