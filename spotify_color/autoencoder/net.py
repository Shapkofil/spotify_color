import torch 
import torch.nn as nn 


class Net(nn.Module):
    def __init__(self) -> None:
        super(Net, self).__init__()

        self.encoder = nn.Sequential(
            nn.Linear(78, 40),
            nn.ReLU(),
            nn.Linear(40, 4)
        )
        self.decoder = nn.Sequential(
            nn.Linear(4, 40),
            nn.ReLU(),
            nn.Linear(40, 78)
        )

    # Defining the forward pass
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.encoder(x)
        x = self.decoder(x)
        return x
