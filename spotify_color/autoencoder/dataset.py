import numpy as np
import torch
import pandas as pd 
from typing import Tuple
from pathlib import Path
from sklearn.preprocessing import MinMaxScaler

class Dataset:
    """
    Dataset object.
    """

    def __init__(self, x: Path, y: Path) -> None:
        # Targets
        self.targets = Dataset.load_numpy_arr_from_pickle(y)
        # Inputs
        self.inputs = Dataset.load_numpy_arr_from_pickle(x)
    
    def __len__(self) -> int:
        return len(self.targets)
    
    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        input = torch.from_numpy(self.inputs[idx]).float()
        target = torch.from_numpy(self.targets[idx]).float()
        return input, target 
    
    @staticmethod 
    def load_numpy_arr_from_pickle(path: Path) -> np.ndarray:
        """
        Load input and target from path + MinMaxScaling. 
        """
        df = pd.read_pickle(path)
        # Scale 
        douma = MinMaxScaler()
        scaled_df = douma.fit_transform(df.to_numpy())
        return scaled_df 

