import os
import argparse
import pandas as pd
import ast
import sys
import numpy as np
import concurrent.futures
import asyncio
from tqdm import tqdm
from functools import partial
from pathlib import Path
from sklearn.preprocessing import StandardScaler, RobustScaler


def normstack(args, obj)->bytes:
    obj = pd.json_normalize(ast.literal_eval(obj))
    if "timbre" not in obj or "pitches" not in obj:
        print("Skipping row because timbre and pitches are missing")
        return None

    shliop = pd.concat([obj.drop(["timbre", "pitches"], axis=1),
                        obj['timbre'].apply(
                            lambda x: pd.Series(x, index=range(0, 12))),
                        obj['pitches'].apply(
                            lambda x: pd.Series(x, index=range(12, 24)))], axis=1)

    shliop = shliop.reset_index(drop=True).astype(np.float32)

    def robust_column(df, cols):
        scaler = RobustScaler()
        return scaler.fit_transform(df[cols])

    shliop[["loudness_start", "loudness_max", "loudness_end"]] = robust_column(
        shliop, ["loudness_start", "loudness_max", "loudness_end"])

    shliop["start"] = robust_column(shliop, ["start"])
    shliop["duration"] = robust_column(shliop, ["duration"])
    shliop["start"] = robust_column(shliop, ["start"])
    shliop["loudness_max_time"] = robust_column(shliop, ["loudness_max_time"])

    # Scaling tembre
    shliop[[col for col in range(12)]] = robust_column(
        shliop, [col for col in range(12)])

    shliop = shliop.reindex(np.arange(args.max_segments), fill_value=0)
    shliop = shliop.to_numpy().tobytes("C")

    return shliop


def parse_file(executor, file_path, output_file, args) -> None:
    """
    Parse a CSV file and apply the desired parsing operations.
    Save the parsed results to the output file in .npy format.
    """
    df = pd.read_csv(file_path, index_col=[0])

    tasks = [executor.submit(normstack, args, row) for row in df["segments"]]
    return tasks


def process_files_parallel(directory, output_file, args):
    """
    Process all CSV files in the given directory using parallel execution.
    """

    with concurrent.futures.ProcessPoolExecutor(max_workers=args.workers) as executor:
        tasks = []
        for file_name in os.listdir(directory):
            if file_name.endswith('.csv'):
                file_path = os.path.join(directory, file_name)
                file_tasks = parse_file(executor, file_path, output_file, args)
                # task = executor.submit(parse_file, file_path, output_file, args)
                tasks[0:0] = file_tasks

        output_file = Path(output_file)
        if output_file.exists():
            if args.override:
                output_file.unlink()
            else:
                raise RuntimeError(
                    "Output file already exists pass -f to override.")
            
        with open(output_file, "ab") as f:
            for future in tqdm(
                    concurrent.futures.as_completed(tasks),
                    total=len(tasks)):
                try:
                    _bytes = future.result()
                except Exception as exc:
                    print('Worker Error: %s' % (exc.with_traceback()))

                f.write(_bytes)


def main():
    parser = argparse.ArgumentParser(description="CSV Parsing Script")
    parser.add_argument("--directory", "-d", type=str,
                        help="Directory containing CSV files")
    parser.add_argument("--output_file", "-o", type=str,
                        help="Output file to store the parsed results in .parquet format")
    parser.add_argument("--max_segments", "-n", default=65536, type=int)
    parser.add_argument("--workers", "-w", default=4, type=int)
    parser.add_argument("--override", "-f", action='store_true')
    args = parser.parse_args()

    process_files_parallel(args.directory, args.output_file, args)

    print("Parsing complete.")


if __name__ == "__main__":
    main()
