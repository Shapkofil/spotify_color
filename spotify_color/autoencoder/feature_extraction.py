import numpy as np 
import pandas as pd 
from typing import *
import argparse
from datetime import datetime 
from pathlib import Path
import os 
import ast 

def extract_track(track: Dict[str, float]) -> pd.Series:
    """
    Drop string keys. Output series.
    """
    remove_list = ['sample_md5', 'codestring', 'echoprintstring', 'synchstring', 'rhythmstring']
    copy_track = track.copy()
    remove_list = [copy_track.pop(key) for key in remove_list] # Inplace 
    return pd.Series(copy_track)


def extract_bars(track_bars: List[Dict[str, float]]) -> pd.Series:
    """
    Calculate avg start, avg duration of bars. Number of bar. Avg conf.
    """
    dct = {
        'avg_bars_start' : np.mean([bar['start'] for bar in track_bars]),
        'avg_bars_duration' : np.mean([bar['duration'] for bar in track_bars]),
        'num_bars' : len(track_bars),
        'avg_bars_duration_conf' : np.mean([bar['confidence'] for bar in track_bars]),
    }
    return pd.Series(dct)


def extract_beats(track_beats: List[Dict[str, float]]) -> pd.Series:
    """
    Calculate avg start, avg duration of bars. Number of bar. Avg conf.
    """
    dct = {
        'avg_beats_start' : np.mean([bar['start'] for bar in track_beats]),
        'avg_beats_duration' : np.mean([bar['duration'] for bar in track_beats]),
        'num_beats' : len(track_beats),
        'avg_beats_duration_conf' : np.mean([bar['confidence'] for bar in track_beats]),
    }
    return pd.Series(dct)


def extract_sections(track_sections: List[Dict[str, float]]) -> pd.Series:
    """
    Avg everthing + number of sections. 
    """
    keys = [key for key in track_sections[0].keys()]
    dct = {}
    for key in keys:
        dct.update({
            'avg_' + key : np.mean([bar[key] for bar in track_sections])
        })
    dct['num_sections'] = len(track_sections)
    
    return pd.Series(dct)    


def extract_segments(track_segments: List[Dict[str, float]]) -> pd.Series:
    """
    Avg everything + avg_chroma_ + avg_timbre_
    """
    keys = [key for key in track_segments[0].keys() if key not in ['pitches', 'timbre']]
    # Avg for normal keys 
    dct = {}
    for key in keys:
        dct.update({
            'avg_' + key : np.mean([bar[key] for bar in track_segments])
        })
    
    # Avg chroma vector 
    avg_chroma = np.mean([segment['pitches'] for segment in track_segments], axis=0)
    for i in range(len(avg_chroma)):
        dct.update({
            'avg_chroma_' + str(i) : avg_chroma[i] 
        })
    
    # Nr. segments 
    dct['num_segments'] = len(track_segments)

    # Avg timbre
    avg_timbre = np.mean([segment['timbre'] for segment in track_segments], axis=0)
    for i in range(len(avg_chroma)):
        dct.update({
            'avg_timbre_' + str(i) : avg_timbre[i] 
        })
    
    return pd.Series(dct)


def extract_tatums(track_tatums: List[Dict[str, float]]) -> pd.Series:
    """
    Avg start time, duration, conf + num_tatums. 
    """
    # if len(track_tatums) == 0:
    #     return pd.Series({'num_tatums' : 0})
    keys = [key for key in track_tatums[0].keys()]
    dct = {}
    for key in keys:
        dct.update({
            'avg_' + key : np.mean([bar[key] for bar in track_tatums])
        })
    dct['num_tatums'] = len(track_tatums)
    
    return pd.Series(dct)    

# Extract features each row 
def extract_features(row: pd.Series) -> pd.Series:
    """
    Perform features extraction on analysis dataframe. 
    """
    features = pd.concat([
        extract_track(row['track']),
        extract_bars(row['bars']),
        extract_beats(row['beats']),
        extract_sections(row['sections']),
        extract_segments(row['segments']),
        extract_tatums(row['tatums']),
    ])

    return features

def transform(df: pd.DataFrame) -> pd.DataFrame:
    """
    Flatten and perform feature extraction. 
    """
    # Get the transform data 
    lst = [] 
    for idx in range(len(df)):
        lst.append(pd.DataFrame(extract_features(df.iloc[idx])).T)
    transform_df = pd.concat(lst, axis=0)
    transform_df.reset_index(drop=True)

    return transform_df

# Main 
def main(args: argparse.Namespace) -> None:
    input_path = args.input
    output_path = args.output 
    df = pd.read_csv(input_path, index_col=[0])
    df = df.drop(columns=['meta'])
    df = df.applymap(ast.literal_eval)
    # Get the transform data 
    lst = [] 
    for idx in range(len(df)):
        lst.append(pd.DataFrame(extract_features(df.iloc[idx])).T)
    transform_df = pd.concat(lst, axis=0)
    transform_df.reset_index(drop=True)
    transform_df.to_pickle(output_path)

if __name__=="__main__":
    # Time 
    now = datetime.now()

    # Path 
    if not Path("data/").exists():
        os.mkdir(Path("data/"))

    # Parser 
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output", 
        help="output path", 
        default=f'data/transform_analysis_{now.month:02}_{now.day:02}_{now.hour}_{now.minute:02}.pk',
        type=str,
    )
    parser.add_argument(
        "--input", help='input path', default='data/analysis.csv', type=str, 
    )
    args = parser.parse_args()
    main(args)

