from tqdm import tqdm 
import torch 
import torch.nn as nn 
from net import Net 
from batch_sampler import BatchSampler 
from typing import *

# Train:
def train_model(
        model: Net, 
        train_sampler: BatchSampler,
        optimizer: torch.optim.Optimizer,
        loss_function: Callable[..., torch.Tensor], 
        device: str):
    # keep track losses 
    losses = []
    # Put the model in train mode:
    model.train()
    # Feed all the batches one by one:
    for batch in tqdm(train_sampler):
        # Get a batch:
        x, y = batch 
        # samples on same device
        x, y = x.to(device), y.to(device)
        # Get predictions:
        predictions = model.forward(x)
        loss = loss_function(predictions, y)
        losses.append(loss)
        # Reset optimizer 
        optimizer.zero_grad()
        # Backprepagate loss
        loss.backward()
        # Step optimizer
        optimizer.step()

    return losses 

# Test: 
def test_model(
        model: Net, 
        test_sampler: BatchSampler, 
        loss_function: Callable[..., torch.Tensor],
        device: str):
    # To evaluation: 
    model.eval()
    losses = []
    with torch.no_grad():
        # Feed all the batches one by one: 
        for (x, y) in tqdm(test_sampler):
            # samples on same device
            x, y = x.to(device), y.to(device)  
            # Get Predictions:
            prediction = model.forward(x) 
            loss = loss_function(prediction, y)
            losses.append(loss)
    
    return losses 
