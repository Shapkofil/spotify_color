from datetime import datetime 
from pathlib import Path 
import os 
import argparse
import pandas as pd 
from sklearn.model_selection import train_test_split

def main(args: argparse.Namespace) -> None:
    input_path = args.input
    output_path = args.output 
    # Split 
    df = pd.read_pickle(input_path)
    X_train, X_test, y_train, y_test = train_test_split(df, df, test_size=0.2, random_state=42)
    # Save 
    output_path = Path(output_path)
    X_train.to_pickle(
        os.path.join(
            output_path.parent,
            'train_' + os.path.split(output_path)[-1]
        )
    )
    X_test.to_pickle(
        os.path.join(
            output_path.parent,
            'test_' + os.path.split(output_path)[-1]
        )
    )



if __name__=="__main__":
    # Time 
    now = datetime.now()

    # Path 
    if not Path("data/").exists():
        os.mkdir(Path("data/"))

    # Parser 
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output", 
        help="output path", 
        default=f'data/transform_{now.month:02}_{now.day:02}_{now.hour}_{now.minute:02}.pk',
        type=str,
    )
    parser.add_argument(
        "--input", help='input path', default='data/transformed.pk', type=str, 
    )
    args = parser.parse_args()
    main(args)