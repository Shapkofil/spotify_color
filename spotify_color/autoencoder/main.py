# Custom imports 
from batch_sampler import BatchSampler
from dataset import Dataset
from train_test import train_model, test_model 
from net import Net

# Torch imports 
import torch 
import torch.nn as nn 
import torch.optim as optim 
from torchsummary import summary 

# Other imports 
import argparse 
import os 
from datetime import datetime 
from pathlib import Path 
from typing import *
import numpy as np 
import pandas as pd 

def main(args: argparse.Namespace, activeloop: bool = True) -> None:
    # Load the train and test dataset 
    train_dataset = Dataset(Path("data/train_transformed.pk"), Path("data/train_transformed.pk"))
    test_dataset = Dataset(Path("data/test_transformed.pk"), Path("data/test_transformed.pk"))
    # Load the Neural Net. 
    model = Net()
    # Fetch learing rate, weight decay, momentum from arguments 
    lr = args.lr
    wdecay = args.wd
    momentum = args.momentum
    # Initialize optimizer(s) and loss function(s)
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=momentum, weight_decay=wdecay)
    loss_function = nn.MSELoss()
    # Fetch epoch and batch count from arguments 
    n_epochs = args.nb_epochs 
    batch_size = args.batch_size 
    # DEBUG
    DEBUG = False 
    # Moving our model to the right device 
    if torch.cuda.is_available() and not DEBUG:
        print("@@@ CUDA device found, enabling CUDA training...")
        device = "cuda"
        model.to(device)
        # Creating a summary of our model and its layers:
        summary(model, (1 ,78), device=device)
    else:
        print("@@@ No GPU boosting device found, training on CPU...")
        device = "cpu"
        # Creating a summary of our model and its layers:
        summary(model, (1, 78), device=device)

    # BatchSampler 
    train_sampler = BatchSampler(
        batch_size=batch_size,
        dataset=train_dataset
    )
    test_sampler = BatchSampler(
        batch_size=10, 
        dataset=test_dataset    
    )   

    # Track losses
    mean_losses_train: List[torch.Tensor] = []
    mean_losses_test: List[torch.Tensor] = []
    # Main learning loop 
    for e in range(n_epochs):
        if activeloop:
            # Training:
            losses_train = train_model(
                model,
                train_sampler, 
                optimizer, 
                loss_function, 
                device
            )
            # Calculate and print statistics:
            mean_loss = sum(losses_train) / len(losses_train)
            mean_losses_train.append(mean_loss)
            print(f"\nEpoch {e + 1} training done, loss on train set: {mean_loss}\n")

            # Testing:
            losses_test = test_model(
                model, 
                test_sampler,
                loss_function,
                device
            )
            # Calculate and print statistics 
            mean_loss = sum(losses_test) / len(losses_test)
            mean_losses_test.append(mean_loss)
            print(f"\nEpoch {e + 1} testing done, loss on test set: {mean_loss}\n")
    
    # Retrieve current time 
    now = datetime.now()
    # check if model_weights/ subdir exists
    if not Path("model_weights/").exists():
        os.mkdir(Path("model_weights/"))
    # Saving the model
    torch.save(model.state_dict(), f"model_weights/model_{now.month:02}_{now.day:02}_{now.hour}_{now.minute:02}.pt")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--nb_epochs", 
        help="number of training iterations", 
        default=10, 
        type=int
    )
    parser.add_argument(
        "--batch_size", 
        help="batch_size", 
        default=10, 
        type=int
    )
    parser.add_argument("--lr", help="learning rate", default=0.001, type=float)
    parser.add_argument("--wd", help="weight decay", default=0.05, type=float)
    parser.add_argument("--momentum", help="momentum", default=0.1, type=float)
    args = parser.parse_args()

    main(args)