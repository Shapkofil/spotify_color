"""
in this file the API class that contains all calls to Spotify API
all methods are calls to one endpoint
the return of all methods is the raw json
"""

from typing import *
import requests
import base64
import urllib.request, urllib.error
import urllib.parse as urllibparse
# get CLIENT_ID, CLIENT_SECRET
from spotify_color.secret import *


# --- VARS --- #
auth_url = 'https://accounts.spotify.com/api/token'
redirect_uri = 'http://139.162.132.192/callback/'
base64_encoded = base64.b64encode(("{}:{}".format(CLIENT_ID,CLIENT_SECRET)).encode())

# steps to obtain redirect link
scope = 'playlist-modify-public playlist-modify-private user-read-recently-played user-top-read playlist-read-private, user-follow-modify, user-follow-read, user-follow-modify, user-read-private, user-library-read, user-read-email'
auth_query_parameters = {"response_type": "code","redirect_uri": redirect_uri,"scope": scope,"client_id": CLIENT_ID}
url_args = "&".join(["{}={}".format(key, urllibparse.quote(val))
                        for key, val in list(auth_query_parameters.items())])
REDIRECT_URL = "https://accounts.spotify.com/authorize/?{}".format(url_args)



# --- AUTH --- #

# TODO handle auth tokens expiring
# TODO error handling

def authorize(auth_token) -> Dict:
    """ given auth_token of active user auth returns header to be used to make requests for that user """
    code_payload = {
                'grant_type': 'authorization_code',
                'code': str(auth_token),
               'redirect_uri': redirect_uri}
    header = {'Authorization': 'Basic {}'.format(base64_encoded.decode())}
    post_request = requests.post(auth_url, data=code_payload, headers=header)
    res = post_request.json()
    access_token = res['access_token']
    refresh_token = res['refresh_token']
    header = {'Authorization': 'Bearer {}'.format(access_token)}
    return header, refresh_token

def refreshAuth(refresh_token) -> Dict:
    """ method that with refresh token refreshes auth token of some user """
    code_payload={
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,}
    req = requests.post(auth_url, data=code_payload, headers={'Authorization': 'Basic' + str(base64encoded)})
    res = req.json()
    access_token = res['access_token']
    header = {'Authorization': f'Bearer {access_token}'}
    return header
