import sqlite3
# one table: 'users', 2 attributes: 'email': primary key, 'token': auth refresh token

# Select all rows from the table and print them

def printTable():
    conn = sqlite3.connect('../data/keys.db')
    c = conn.cursor()
    [print(row) for row in c.execute("SELECT * FROM users")]
    conn.close() 

def clearTable():
    conn = sqlite3.connect('../data/keys.db')
    c = conn.cursor()
    c.execute("DELETE FROM users")
    conn.commit()
    conn.close()


def addUser(mail, token):
    conn = sqlite3.connect('../data/keys.db')
    c = conn.cursor()

    # Try updating the refresh token of an existing user might be redundant
    c.execute("UPDATE users SET token='%s' WHERE email='%s';" % (token, mail))

    # Try to add the user ignore is key is not unique
    c.execute("INSERT OR IGNORE INTO users (email, token) VALUES (?, ?);", (mail, token))

    conn.commit()
    conn.close()


if __name__ == '__main__':
    printTable()
