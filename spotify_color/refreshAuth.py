import requests
from secret import base_64, refresh_token


class Refresh:
    """class that refreshes the auth token that expires every hour"""
    def __init__(self):
        # Token got from web auth step
        self.refresh_token = refresh_token
        # client_id:client_secret base64 encoded
        self.base64 = base_64

    def refresh(self):
        uri = 'https://accounts.spotify.com/api/token'
        req = requests.post(uri,
                            data={'grant_type': 'refresh_token', 'refresh_token': self.refresh_token},
                            headers={'Authorization': 'Basic ' + self.base64})

        # print(f'Access token: {req.json()}')
        req_json = req.json()
        return req_json['access_token']


if __name__ == '__main__':
    a = Refresh()
    a.refresh()