"""
this file contains all functions that return the raw json of a call to one api endpoint
"""

import requests

url = 'https://api.spotify.com/v1/'


# --- USER --- #

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-current-users-profile
def getUser(header):
    """ 
    /me endpoint
    OAuth2.0 gets user information, needs Bearer token
    """
    req = requests.get(url+'me', headers=header)
    return req.json()
    

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-users-top-artists-and-tracks
def getUserTop(header, rtype='tracks', limit=50, time_range='long_term', offset=0):
    """
    /me/top/{type}
    OAuth2.0 gets (50 per request) user top items, needs Bearer token
    rtype: 'tracks', 'artists'
    time_range: 'long_term', 'medium_term', 'short_term'
    offset: returns items from index offset
    limit: number of items to return per request, max 50
    """
    req = requests.get(url+'me/top/'+rtype, params={'limit': limit, 'offset': offset, 'time_range': time_range}, headers=header)
    try:
        return req.json()
    except Exception:
         raise TypeError(req, req.text())

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-users-saved-tracks
def getUserSavedTracks(header, limit=50, offset=0):
    """
    /me/tracks
    OAuth2.0 gets user saved tracks, needs Bearer token
    offset: returns items from index offset
    limit: number of items to return per request, max 50
    """
    req = requests.get(url+'me/tracks/', params={'limit': limit, 'offset': offset}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-recently-played
def getRecentlyPlayed(header, before=None, after=None, limit=50):
    """
    /me/player/recently-played
    OAuth2.0 gets user recently played tracks, needs Bearer token
    before/after: A Unix timestamp in milliseconds. Returns all items after (but not including) this cursor position. if one is specified the other must not be specified
    limit: number of items to return per request, max 50
    """
    if after is None and before is not None:
        req = requests.get(url + 'me/player/recently-played', params={'before': before, 'limit': limit}, headers=header)
    elif after is not None and before is None:
        req = requests.get(url + 'me/player/recently-played', params={'after': after, 'limit': limit}, headers=header)
    else:
        req = requests.get(url + 'me/player/recently-played', params={'limit': limit}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-queue
def getUserQueue(header):
    raise NotImplementedError


# --- ARTISTS --- #

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-followed
def getFollowedArtists(header, limit=50, after=None):
    """
    /me/following
    OAuth2,0 gets user followed artists, needs Bearer token
    after: The last artist ID retrieved from the previous request
    limit: the number of items to return per request, max 50
    """
    if after is None:
        req = requests.get(url + 'me/following', params={'type': 'artist', 'limit': limit}, headers=header)
    else:
        req = requests.get(url + 'me/following', params={'type': 'artist', 'after': after, 'limit': limit}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-multiple-artists
def getArtists(header, ids):
    """
    /artists
    OAuth2.0, gets list of artists, needs Authentication
    ids: list of artist ids, max 50
    """
    s = [idx+',' for idx in ids]
    s = s[:-1]
    req = request.get(url+'artists', params={'ids': s}, header=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-an-artists-top-tracks
def getArtistTop(header, aid, country_code='NL'):
    """
    /artist/{id}/top-tracks
    OAuth2.0 get artist to tracks in a country, needs Authentication
    aid: artist id
    country_code: get top tracks in said country
    """
    req = requests.get(url+'artists'+aid+'/top-tracks/', params={'market':country_code}, headers=header)
    return req.json()


# --- PLAYLISTS --- #

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-list-users-playlists
def getUserPlaylists(header, limit=50, offset=0):
    """
    /me/playlists
    OAuth2.0 get user playlists, needs Bearer token
    offset: returns items from index offset
    limit: number of items to return per request, max 50
    """
    req = requests.get(url+'me/playlists', params={'limit': limit, 'offset': offset}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-playlists-tracks
def getPlaylistItems(header, pid, limit=50, offset=0):
    """
    /playlists/{playlist_id}/tracks
    OAuth2.0 gets tracks from playlist, needs Authentication
    offset: returns items from index offset
    limit: number of items to return per request, max 50
    """
    req = requests.get(url+'playlists/'+pid+'/tracks', params={'limit': limit, 'offset': offset}, headers=header)
    return req.json()
    

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-featured-playlists
def getFeaturedPlaylists(header, country_code='US', offset=0, limit=50, timestamp=None, locale='en_US'):
    """
    /browse/featured-playlists
    OAuth2.0 gets a list of spotify featured playlists, requires Authentication
    offset: returns items from index offset
    limit: number of items to return per request, max 50
    timestamp: A timestamp in ISO 8601 format  to specify the user's local time to get results tailored for that specific date and time in the day
    """
    if timestamp is None:
        req = requests.get(url+'browse/featured-playlists', params={'offset': offset, 'limit': limit, 'locale': locale, 'country': country_code}, headers=header)
    else:
        req = requests.get(url+'browse/featured-playlists', params={'offset': offset, 'limit': limit, 'locale': locale, 'country': country_code, 'timestamp': timestamp}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-a-categories-playlists
def getCategoryPlaylist(header):
    raise NotImplementedError


# --- ANALYSIS --- #

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-several-audio-features
def getTrackAudioFeatures(header, ids):
    """
    /audio-features
    OAuth2.0 get tracks audio features, needs Authentication
    ids: list of max 100 comma separeted str of id strings
    """
    req = requests.get(url + 'audio-features', params={'ids': ids}, headers=header)
    return req.json()


# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-audio-analysis
def getTrackAudioAnalysis(header, tid):
    """
    /audio-analysis/{id}
    OAuth2.0 get tracks low-level audio analysis, needs Authentication
    tid: track id 
    """
    req = requests.get(url+'audio-analysis/'+tid, headers=header)
    return req.json()


def getTrack(header, tid):
    """
    /audio-analysis/{id}
    OAuth2.0 get tracks low-level audio analysis, needs Authentication
    tid: track id 
    """
    req = requests.get(url+'tracks/'+tid, headers=header)
    return req.json()



# --- SEARCH & RECOMMEND --- #

def getRecommendations(header):
    raise NotImplementedError

def searchItem(header, query: str, dtype='track', limit=5):
    """
    /search
    OAuth2.0 search spotify catalogue, needs Authentication
    query: your search query
    type: 'album', 'artist', 'track', 'genre'
    limit: The maximum number of results to return in each item type, max 50
    """
    req = requests.get(url+'search', params={'q': query, 'type': dtype, 'limit': limit}, headers=header)
    return req.json()
