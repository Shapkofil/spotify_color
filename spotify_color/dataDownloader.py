"""
here is the class that handles requests to the spotify API,
all the class methods are calls to an API endpoint
the return of all methods is the raw json returned
simple error handling is supported but better eh is #TODO
"""

from os import register_at_fork
import requests
import secret
from typing import *
import time
import datetime
import base64
import urllib.request, urllib.error
import urllib.parse as urllibparse


def handleErrors(req, name) -> bool:
    """function that gets the requests.module.response obj as input and returns bool (True = errors happened)"""
    # malformed json
    try:
        r = req.json()
    except Exception:
        print(f'Unknown error in {name}\n{req}')
        return True
    # regular errors catch
    if list(r.keys()) == ['error']:
        # error of some kind got returned
        print(f'error: {r["error"]["status"]}\ndescription: {r["error"]["message"]}')
        return True
    # if no error is catched return False
    return False
    # TODO this probably doesnt catch everything, better error handling


class SpotiDownloader:
    """
    class that instantiates a client connected to the spoti API,
    reads credentials from secret.py, refreshes auth token and makes it available to the class methods for querying
    """
    def __init__(self):
        """init, from secret.py get CLIENT_ID"""
        self.client_id = secret.CLIENT_ID
        self.client_secret = secret.CLIENT_SECRET
        self.auth_url = 'https://accounts.spotify.com/'
        self.token = None
        self.url = 'https://api.spotify.com/v1/'
        self.header = None
        self.client_side_url = 'http://139.162.132.192'
        self.redirect_uri = f'{self.client_side_url}/callback/'
        self.scope = 'playlist-modify-public playlist-modify-private user-read-recently-played user-top-read playlist-read-private, user-follow-modify, user-follow-read, user-follow-modify, user-read-private, user-library-read, user-read-email'
        self.base64encoded = None
        self.refreshToken = None
        self.getUrlArgs()

    def getUrlArgs(self):
        auth_query_parameters = {
            "response_type": "code",
            "redirect_uri": self.redirect_uri,
            "scope": self.scope,
            # "state": STATE,
            # "show_dialog": SHOW_DIALOG_str,
            "client_id": self.client_id}
        self.url_args = "&".join(["{}={}".format(key, urllibparse.quote(val))
                    for key, val in list(auth_query_parameters.items())])

    def authorize(self, auth_token):
        code_payload = {
                'grant_type': 'authorization_code',
                'code': str(auth_token),
                'redirect_uri': self.redirect_uri}
        base64_encoded = base64.b64encode(("{}:{}".format(self.client_id,self.client_secret)).encode())
        header = {'Authorization': 'Basic {}'.format(base64_encoded.decode())}
        post_request = requests.post(self.auth_url+'api/token', data=code_payload, headers=header)
        res = post_request.json()
        access_token = res['access_token']
        self.refresh_token = res['refresh_token']
        self.header = {'Authorization': 'Bearer {}'.format(access_token)}
        return self.header


    def refreshAuth(self):
        if self.refreshToken is not None:
            req = requests.post(self.auth_url+'api/token', data={'grant_type': 'refresh_token', 'refresh_token': self.refreshToken}, headers={'Authorization': 'Basic ' + str(self.base64encoded)})
            res = req.json()
            self.token = res['access_token']
            self.header = {'Authorization': f'Bearer {self.token}'}
        else:
            raise TypeError
        return self.header

    def getUser(self):
        """ /me API endpoint """
        req = requests.get(self.url + 'me', headers={'Authorization': f'Bearer {self.token}'})
        # error check
        if handleErrors(req, 'getUser'):
            return None
        res = req.json()
        return res

    def getAllUserTop(self, what, time_range='long_term') -> List[Dict]:
        """ calls /top for every element in it"""
        res = []
        try:
            total = self.getUserTop()['total']
        except TypeError:
            return None
        x, rest = divmod(total, 50)
        for i in range(x):
            res.append(self.getUserTop(what=what, time_range=time_range, offset=i*50))
        if rest != 0:
            res.append(self.getUserTop(what=what, time_range=time_range, offset=x*50))
        return res

    def getUserTop(self, what='tracks', limit=50, time_range='long_term', offset=0):
        """ me/top API endpoint """
        # what= 'tracks' or 'artists' | time_range='long_term' or 'medium_term' or 'short_term'
        req = requests.get(self.url + 'me/top/' + what, params={'limit': limit, 'offset': offset, 'time_range': time_range}, headers=self.header)
        # error check
        if handleErrors(req, 'getUserTop'):
            return None
        result = req.json()
        return result

    def getAllUserSavedTracks(self) -> List[Dict]:
        """ calls getsavedtracks until we get all saved"""
        res = []
        try:
            total = self.getUserSavedTracks()['total']
        except TypeError:
            return None
        x, rest = divmod(total, 50)
        for i in range(x):
            res.append(self.getUserSavedTracks(limit=50, offset=i*50))
        if rest != 0:
            res.append(self.getUserSavedTracks(limit=rest, offset=x*50))
        return res

    def getUserSavedTracks(self, limit=50, offset=0):
        """/me/tracks API endpoint"""
        req = requests.get(self.url + 'me/tracks/', params={'limit': limit, 'offset': offset}, headers=self.header)
        # error check
        if handleErrors(req, 'getUserSavedTracks'):
            return None
        res = req.json()
        return res

    def getAllUserPlaylists(self):
        """ calls getuserplaylist until we get all"""
        res = []
        try:
            total = self.getUserPlaylists()['total']
        except TypeError:
            return None
        x, rest = divmod(total, 50)
        for i in range(x):
            res.append(self.getUserPlaylists(limit=50, offset=i * 50))
        res.append(self.getUserPlaylists(limit=rest, offset=x * 50))
        return res

    def getUserPlaylists(self, limit=50, offset=0):
        """ /me/playlists API enpoint """
        req = requests.get(self.url + 'me/playlists', params={'limit': limit, 'offset': offset},
                           headers={'Authorization': f'Bearer {self.token}'})
        # error check
        if handleErrors(req, 'getUserPlaylists'):
            return None
        res = req.json()
        return res

    def getPlaylistItems(self, id, limit=50, offset=0):
        req = requests.get(self.url + 'playlists/'+ id +'/tracks', params={'limit': limit, 'offset': offset}, headers=self.header)
        # error check
        if handleErrors(req, 'getPlaylistsItems'):
            return None
        res = req.json()
        return res

    def getAllRecentlyPlayed(self):
        """ calls getrecentlyplayed until we get all recently played"""
        r = []
        now = int(datetime.datetime.timestamp(datetime.datetime.now()) * 1000)  # 13 digit ms unix timestamp (int) as time
        res = self.getRecentlyPlayed(before=now, limit=5)
        if res is None:
            return None
        r.append(res)
        # print(res['cursors']['after'])
        timestamp = res['cursors']['before']
        flag = True
        while flag:
            res = self.getRecentlyPlayed(before=timestamp)
            r.append(res)
            try:    # when there are no more tracks in history API returns None as cursors
                timestamp = res['cursors']['after']
            except TypeError:
                flag = False
        return r

    def getRecentlyPlayed(self, before=None, after=None, limit=50):
        """/me/player/recently-played API endpoint"""
        # TODO better error handling (if after not None, set before to None and viceversa)
        if after is None and before is not None:
            req = requests.get(self.url + 'me/player/recently-played', params={'before': before, 'limit': limit}, headers=self.header)
        elif after is not None and before is None:
            req = requests.get(self.url + 'me/player/recently-played', params={'after': after, 'limit': limit}, headers=self.header)
        else:
            req = requests.get(self.url + 'me/player/recently-played', params={'limit': limit}, headers=self.header)
        # error check
        if handleErrors(req, 'getRecentlyPlayed'):
            return None
        res = req.json()
        return res

    def getTrackAudioFeatures(self, ids: str):
        """/audio-features API endpoint"""
        # ids comma separated str of id strings, max len = 100
        # TODO catch if list > 100 ids
        req = requests.get(self.url + 'audio-features', params={'ids': ids}, headers=self.header)
        # error check
        if handleErrors(req, 'getTrackAudioFeatures'):
            return None
        res = req.json()
        return res

    def getTrackAudioAnalysis(self, id: str):
        """/audio-analysis/ API endpoint, NOTE: only one track per request: #TODO cache results, and limit requests to the API"""
        # id is only one string
        req = requests.get(self.url + 'audio-analysis/' + id, headers=self.header)
        # error check
        if handleErrors(req, 'getTrackAudioAnalysis'):
            return None
        res = req.json()
        return res

    def getAllFollowedArtists(self):
        """calls get followed artists untill we get all of them"""
        res = []
        r = self.getFollowedArtists()
        if r is not None:
            res.append(r)
            try:
                cursor = res[0]['artists']['cursors']['after']
            except ValueError:
                cursor = None
            while cursor is not None:
                res.append(self.getFollowedArtists(after=cursor))
                try:
                    cursor = res[0]['artists']['cursors']['after']
                except ValueError:
                    cursor = None
            return res

    def getFollowedArtists(self, limit=50, after=None):
        """me/following API endpoint"""
        if after is None:
            req = requests.get(self.url + 'me/following', params={'type': 'artist', 'limit': limit}, headers=self.header)
        else:
            req = requests.get(self.url + 'me/following', params={'type': 'artist', 'after': after, 'limit': limit}, headers=self.header)
        # error check
        if handleErrors(req, 'getFollowedArtists'):
            return None
        res = req.json()
        return res

    def getArtistTop(self, id: str, country_code='NL'):
        """artists/{id}/top-tracks/ API endpoint"""
        req = requests.get(f'{self.url}artists/{id}/top-tracks/', params={'market': country_code}, headers=self.header)
        # error check
        if handleErrors(req, 'getArtistTop'):
            return None
        res = req.json()
        return res

    def getArtists(self, ids:List[str]):
        """/artists API endpoint"""
        s = ''
        for id in ids:
            s += id
            s += ','
        s = s[:-1]
        req = requests.get(f'{self.url}artists', params={'ids': s}, headers=self.header)
        # error check
        if handleErrors(req, 'getArtists'):
            return None
        res = req.json()
        return res

    def getUserQueue(self):
        raise NotImplementedError

    def getFeaturedPlaylists(self):
        raise NotImplementedError

    def getCategoryPlaylists(self):
        raise NotImplementedError

    def getRecomendations(self):
        raise NotImplementedError

    def searchItem(self, query: str, type='track', limit=3):
        req = requests.get(f'{self.url}search', params={'q': query, 'type': type, 'limit': limit}, headers=self.header)
        res = req.json()
        return res
