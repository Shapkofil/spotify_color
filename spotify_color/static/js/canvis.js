
var gcl = 1;

function fillSegm(ctx, s, d, f, w, h, inc){
    ctx.fillStyle = colors[gcl];
    gcl = (gcl + 1) % colors.length; 
    ctx.fillRect(  s * w, f * h , d * w,(f + inc) * h);
}

function fillLabel(ctx, label , f, h, inc){
    ctx.fillStyle = "#000000";
    ctx.font = `bold ${inc*h*.8}px Circular`;
    const capitalized =
	  label.charAt(0).toUpperCase()
	  + label.slice(1)
  
    ctx.fillText(capitalized,0,(f + inc) * h);
}

function fillPiano(ctx, w, h, m){


    const keynames = [
	"C",
	"C#",
	"D",
	"D#",
	"E",
	"F",
	"F#",
	"G",
	"G#",
	"A",
	"A#",
	"B",
    ]

    const n = 12;
    const wn = 7;
    const widthb = 1.0 - 1.0;
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#000000";
    ctx.font = `bold ${1/n*h*.8}px Circular`;

    let x=0;
    // for (let i=0; i<n; i+=1) {
    // 	ctx.strokeRect((x-0/wn)*w, 0*h,
    // 		       (1/wn)*w, 1*h);
    // 	x += 1/wn;
    // }
    // 
    // x=0;
    for (let i=0; i<n; i+=1) {

	if(!(i == 0 ||
	     i == 2 ||
	     i == 4 ||
	     i == 5 ||
	     i == 7 ||
	     i == 9 ||
	     i == 11) 
	  )
	{
	    ctx.fillRect( (x+(0 + widthb)/n)*w,  m*h,
			  (  (1 - 2.0*widthb)/n)*w, (.66*1 + (-.66)*m)*h);

	}
	else if(i==0 || i == 5)
	{ ctx.strokeRect( (x-0.0/n)*w, m*h,
			  0, 1.0*h);
	}
	else
	{ ctx.strokeRect( (x-.5/n)*w, m*h,
			  0, 1.0*h);
	}

	x += 1.0/n;
    }

    x=0;

    for (let i=0; i<n; i+=1) {
	ctx.fillStyle = "#000000";

	if(!(i == 0 ||
	     i == 2 ||
	     i == 4 ||
	     i == 5 ||
	     i == 7 ||
	     i == 9 ||
	     i == 11) 
	  ){
	    ctx.fillStyle = "#ffffff";
	    ctx.fillText(keynames[i], (x+(0 + widthb)/n)*w,  (.66*1 + (1.0-.66)*m)*h);
	}
	else if(i == 0 || i == 5){
	    ctx.fillText(keynames[i], (x+0.0/n)*w,  h);
	}
	else{
	    ctx.fillText(keynames[i], (x-0.5/n)*w,  h);
	}

	x += 1.0/n;
    }



    ctx.strokeRect(0, m*h,
		   w, (1.0-m)*h);

    
}

function histogram(ctx, data, w, h, m, p){


    const n = data.length;

    let x = 0.0;
    for (let i=0; i<n; i+=1) {
	ctx.fillStyle = colors[gcl];
	gcl = (gcl + 1) % colors.length; 

	ctx.fillRect( (x+(0 + p)/n)*w,  (1-data[i])*m*h,
		      (  (1 - 2.0*p)/n)*w,  data[i]*m*h);

	x += 1.0/n;
    }

}

window.addEventListener("load", () => {
    

    // Treevis plot
    {const canvas = document.getElementById("treevis");
    if (canvas.getContext) {
	
	const width = canvas.width;
	const height = canvas.height;
	const ctx = canvas.getContext("2d");
	const fields = ["sections",
			"bars",
			"beats",
			"segments",
			"tatums" ];

	const r = .7;
	const a = (1-r)/(1-Math.pow(r,fields.length));

	let inc = a;
	let iter = 0.0;

	for (const field of fields) {
	    for (const cs of starts[field]){
		fillSegm(ctx, cs.start, cs.duration, iter, width, height, inc);
	    }
	    iter += inc;
	    inc *= r;
	}

	inc = a;
	iter = 0.0;

	for (const field of fields) {
	    fillLabel(ctx, field, iter, height, inc);
	    iter += inc;
	    inc *= r;
	}
    }}



    // Chromavis plot
    {const canvas = document.getElementById("keyvis");
    if (canvas.getContext) {
	
	const width = canvas.width;
	const height = canvas.height;
	const ctx = canvas.getContext("2d");

	fillPiano(ctx, width, height, .6);
	histogram(ctx, chroma, width, height, .6, .15);
	
    }}

});
