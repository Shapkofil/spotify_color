import numpy as np
import pandas as pd
from spotify_color import api
from datetime import datetime
import os
import asyncio


def getAllSavedTracks(header, name):
    """ stores raw json from getUserSavedTracks """

    total = api.getUserSavedTracks(header)['total']
    x, rest = divmod(total, 50)
    for i in range(x):
        store(api.getUserSavedTracks(header, limit=50, offset=i*50),'savedTracks', name)
    if rest != 0:
        store(api.getUserSavedTracks(header, limit=50, offset=x*50), 'savedTracks', name)


def getTop(header, name, time_range='short_term', what='tracks'):
    """ 
    stores raw json from getTopTracks,
    time_range: 'short_term', 'medium_term', 'long_term'
    what: 'artists', 'tracks'
    """
    total = api.getUserTop(header, rtype=what, time_range=time_range)['total']
    x, rest = divmod(total, 50)
    for i in range (x):
        store(api.getUserTop(header, rtype=what, time_range=time_range, offset=i*50), f'top{what}', name)
    if rest != 0:
        store(api.getUserTop(header, rtype=what, time_range=time_range, offset=x*50), f'top{what}', name)

def getPlaylistTracks(header, name):
    """
    gets list of all playlists and downloads all tracks from them
    """
    playlists = []
    total = api.getUserPlaylists(header)['total']
    x, rest = divmod(total, 50)
    for i in range(x):
        playlists.append(api.getUserPlaylists(header, limit=50, offset=i*50))
    playlists.append(api.getUserPlaylists(header, limit=50, offset=x*50))
    for request in playlists:
        for playlist in request['items']:
            store(api.getPlaylistItems(header, playlist['id']), 'playlistTracks', name)


def getRecentlyPlayed(header, name):
    """ gets all recently played tracks """
    now = int(datetime.timestamp(datetime.now()) * 1000)
    res = api.getRecentlyPlayed(header, before=now, limit=5)
    store(res, 'recentlyPlayed', name)
    timestamp = res['cursors']['before']
    flag = True
    while flag:
        res = api.getRecentlyPlayed(header, before=timestamp)
        store(res, 'recentlyPlayed', name)
        try: # when there are no more tracks in history API returns None as cursors
            timestamp = res['cursors']['after']
        except TypeError:
            flag = False


def getFollowedArtists(header, name):
    """ gets all followed artists """
    r = api.getFollowedArtists(header)
    store(r, 'followedArtist', name)
    try: 
        cursor = r['artists']['cursors']['after']
    except ValueError:
        cursor = None
    while cursor is not None:
        res = api.getFollowedArtists(header, after=cursor)
        store(res, 'followedArtist', name)
        try:
            cursor = res[0]['artists']['cursors']['after']
        except ValueError:
            cursor = None



def store(rec, fname, name):
    """ 
    void that saves json from any function is users folder.
    as read by DataFrame.from_dict in parquet format. 
    """
    timestamp = datetime.now().strftime("%y-%m-%d-%H-%M")
    df = pd.DataFrame.from_dict(rec)
    try:
        df.to_parquet(f"../data/{name}/{num}/{fname}-{timestamp}.parquet")
    except ValueError:
        df.to_csv(f"../data/{name}/{num}/{fname}-{timestamp}.csv")


def getNum(name):
    if not os.path.exists(f'../data/{name}'):
        os.makedirs(f'../data/{name}')
    dirs = [int(f.split('-')[0]) for f in os.listdir(f'../data/{name}') if os.path.isdir(os.path.join(f'../data/{name}', f))]
    num = max(dirs)+1 if len(dirs) !=0 else 1
    os.makedirs(f'../data/{name}/{num}')
    return num


def main(header, name):
    """ call to dawnload data from all endpoints """
    global num
    num = getNum(name)
    getAllSavedTracks(header, name)
    getTop(header, name, time_range='short_term', what='tracks')
    getTop(header, name, time_range='medium_term', what='tracks')
    getTop(header, name, time_range='long_term', what='tracks')
    getTop(header, name, time_range='short_term', what='artists')
    getTop(header, name, time_range='medium_term', what='artists')
    getTop(header, name, time_range='long_term', what='artists')
    getPlaylistTracks(header, name)
    getFollowedArtists(header, name)
