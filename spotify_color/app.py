'''
        Flask demo app
'''

from flask import Flask, request, redirect, g, url_for, render_template, session

from spotify_color.auth import *

from spotify_color import api
from spotify_color import vis
from spotify_color import dataYanker
from spotify_color import keyServer


# init flask
app = Flask(__name__)
app.secret_key = 'oogaboogacoccobello'

# ----------------------- AUTH API PROCEDURE -------------------------

@app.route("/auth")
def auth():
    return redirect(REDIRECT_URL)


@app.route("/callback/")
def callback():
    auth_token = request.args['code']
    auth_header, refresh_token = authorize(auth_token)
    session['auth_header'] = auth_header
    session['name'] = api.getUser(auth_header)['email']
    # store refresh token
    keyServer.addUser(session['name'], refresh_token)

    app.logger.warning(f'auth_header: {auth_header}')
    return profile()

def valid_token(resp):
    return resp is not None and not 'error' in resp

# -------------------------- API REQUESTS ----------------------------


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/search/')
def search():
    try:
        search_type = request.args['search_type']
        name = request.args['name']
        return make_search(search_type, name)
    except:
        return render_template('search.html')


@app.route('/dash/<trackid>')
def dash(trackid):
    if 'auth_header' in session:
        auth_header = session["auth_header"]
        analJSON = api.getTrackAudioAnalysis(auth_header,
                                             trackid)
        featJSON = api.getTrackAudioFeatures(auth_header,
                                             trackid)
        trackJSON = api.getTrack(auth_header,
                                 trackid)

        if not trackJSON["album"]["images"] is None:
            colors = vis.generate_colors(trackJSON["album"]["images"][0]["url"])
        else:
            colors = vis.generate_colors()

        graphJSON = vis.radar_json(featJSON, colors[0])

        startVectors = vis.gen_durr_arrays(analJSON)
        chromaVector = vis.gen_chroma(analJSON)

        return render_template("dash.html",
                               graphJSON=graphJSON,
                               analJSON=analJSON,
                               startVectors=startVectors,
                               chromaVector=chromaVector,
                               colors=colors,
                               trackJSON=trackJSON,
                               trackname=trackJSON["name"])
    
    return redirect(url_for('index'))


@app.route('/search/<search_type>/<name>')
def search_item(search_type, name):
    return make_search(search_type, name)


def make_search(search_type, name):
    if search_type not in ['artist', 'album', 'playlist', 'track']:
        return render_template('index.html')

    data = api.search(query=name, dtype=search_type)
    api_url = data[search_type + 's']['href']
    items = data[search_type + 's']['items']

    return render_template('search.html',
                           name=name,
                           results=items,
                           api_url=api_url,
                           search_type=search_type)


#@app.route('/artist/<id>')
#def artist(id):
#    artist = spotify.get_artist(id) # TODO
#
#    if artist['images']:
#        image_url = artist['images'][0]['url']
#    else:
#        image_url = 'http://bit.ly/2nXRRfX'
#
#    tracksdata = spotify.get_artist_top_tracks(id) # TODO
#    tracks = tracksdata['tracks']
#
#    related = spotify.get_related_artists(id) # TODO
#    related = related['artists']

#    return render_template('artist.html',
#                           artist=artist,
#                           related_artists=related,
#                           image_url=image_url,
#                           tracks=tracks)


@app.route('/profile')
def profile():
    if 'auth_header' in session:
        auth_header = session['auth_header']
        # get profile data
        profile_data = api.getUser(auth_header)
        # get user playlist data
        playlist_data = api.getUserPlaylists(auth_header)
        # get user recently played tracks
        recently_played = api.getRecentlyPlayed(auth_header)
        #session['name'] = profile_data['email']
        
        if valid_token(recently_played):
            return render_template("profile.html",
                                   user=profile_data,
                                   playlists=playlist_data["items"],
                                   recently_played=recently_played["items"]
                                   )
    return render_template('profile.html')


@app.route('/yank')
def yank():
    # TODO make downloading data async
    # download all data & store
    dataYanker.main(session['auth_header'], session['name'])
    return redirect(url_for('profile'))



@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/featured_playlists')
def featured_playlists():
    if 'auth_header' in session:
        auth_header = session['auth_header']
        hot = api.getFeaturedPlaylists(auth_header)
        if valid_token(hot):
            return render_template('featured_playlists.html', hot=hot)

    return render_template('profile.html')

def startlocal():
    app.run(debug=True, host='localhost', port=8421)

def start():
    app.run(debug=True, host='139.162.132.192', port=80)


if __name__ == "__main__":
    start()
