import numpy as np

# This function takes in a numpy array x and returns a numpy array
# where each element has been transformed by the logistic function.
def revsig(x):
    return np.log(x/(1-x))

def find_A(X,y):
    """This function takes in a numpy array X and a numpy array y and returns the matrix A that solves the equation XA = y.
    Before that, it transforms X and y using the logistic function.
    
    Input: X - numpy array representing the characteristics of the songs with dimensionality m x n
           y - numpy array representing the target variable with dimensionality m x 3
           
    Output: A - numpy array representing the matrix that solves the equation XA = y"""

    # transform X and y using the logistic function
    y = revsig(y)

    # find the matrix A
    return np.linalg.inv(X.T @ X) @ X.T @ y


T = np.random.random((10,10))
Tstar = np.random.random((10,3))
find_A(T, Tstar)
